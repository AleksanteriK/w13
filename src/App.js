import logo from './logo.svg';
import './App.css';

function App() {
  const days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
  const weekdays = days.map((day, index)=> {
     
    return <tr><td key={index}>{day}</td></tr>

  })

  return (
    <div className="App">
      <table>
          <thead>
            <tr>
              <th>Weekday</th>
            </tr>
          </thead>
          <tbody>
              {weekdays}
          </tbody>
        </table>
    </div>
  );
}

export default App;
